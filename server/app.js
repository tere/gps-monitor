var express = require('express');
var cors = require('cors');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
const mongoose = require('mongoose');

const mongoAppDbName = "gps-monitor";
const mongo = {
	creds: 'username:password', //replace with your own
	hosts: [
		'cluster0-shard-00-00-atttm.mongodb.net:27017',
		'cluster0-shard-00-01-atttm.mongodb.net:27017',
		'cluster0-shard-00-02-atttm.mongodb.net:27017'
	]
}

//andreiterecoasa - Tere-1993
console.log(`Running in ${process.env.ENV} mode!!`);
let mongoURI = `mongodb://${mongo.creds}@${mongo.hosts[0]}/${mongoAppDbName}?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin`;

mongoose.Promise = global.Promise;
const connectionPromise = mongoose.connect(mongoURI, {
	useMongoClient: true
});

connectionPromise.then((db) => {
	console.log(` ==== Connected to database ==== `);
}).catch((err) => {
	console.error(`Rejected promise: ${err}`);
	mongoose.disconnect();
});



//init mongo models;

require("./models/device");
require("./models/location");
require("./models/user");


var app = express();
app.use(cors("*"));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

require("./routes/main")(app);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// error handler
app.use(function (err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};

	// render the error page
	res.send(err.status || 500);
	// res.render('error');
});

module.exports = app;
