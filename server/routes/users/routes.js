let Users = require('./users');
let doResponse = require('../../utils/common').doResponse;


module.exports = (app, router) => {
    router.post('/user/connect', (req, res) => {
        Users.connect(req.body).then(r => {
            return res.send(doResponse(r));
        }).catch(r => {
            return res.send(doResponse(r.code, r.message));
        })
    });
}