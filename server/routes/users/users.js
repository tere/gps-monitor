let users = require('../../models/user');
let Devices = require('../devices/devices');
let md5 = require('md5');
const passphrase   = 'welivetodieanothesrday';
const jwt          = require('jsonwebtoken');

let UsersController = {
    connect: (connectRequest) => {
        return new Promise((resolve, reject) => {
            console.log(connectRequest);
            let userCheck = new Promise((userResolve, userReject) => {
                UsersController.getUser(connectRequest.userData.email).then((user) => {
                    userResolve(user);
                }).catch(reason => {
                    UsersController.registerUser(connectRequest.userData.email, connectRequest.userData.password).then(user => {
                        userResolve(user);
                    }).catch(reason => {
                        userReject(reason);
                    })
                })
            });
        
            userCheck.then(user => {
                Devices.getDevice(connectRequest.deviceID).then(device => {
                    return resolve(UsersController.signUserConnect(user));
                }).catch(reason => {
                    Devices.registerDevice(connectRequest.deviceID, user._id, "deviceName").then(device => {
                        return resolve(UsersController.signUserConnect(user));
                    }).catch(reason => {
                        console.log(reason);
                        return reject({code: 501, message: 'Could not register device'});
                    })
                })
            }).catch(reason => {
                console.log(reason);
                return reject({code: 502, message: reason});
            })
        });
    },
    signUserConnect: (user) => {
        return {
            userID: user._id,
            email: user.email,
            token: jwt.sign({
                email: user.email,
                userID: user._id,
            }, passphrase)
        }
    },
    getUser: (email) => {
        return new Promise((resolve, reject) => {
            users.findOne({email}).exec().then(user => {
                if(user) {
                    return resolve(user);
                } 
                return reject(null);
            }).catch(reason => {
                return reject(null);
            })
        })
    },
    registerUser: (email, password) => {
        return new Promise((resolve, reject) => {
            console.log(email, password);
            users.create({email, password: md5(password)},(err, user) => {
                if(err) {
                    return reject(err);
                }
                return resolve(user);
            });
        })
    },
    authorize: (req, res, next) => {
		var token = req.body.token || req.query.token || req.headers['authorization'] || req.headers['x-access-token'];
		// decode token
		if (token) {
			// verifies secret and checks exp
			jwt.verify(token, passphrase, function (err, decoded) {
				if (err) {
                    console.log(err);
					return res.send({code:401, message: 'Failed to authenticate token.' })
				} else {
					// if everything is good, save to request for use in other routes
					req.decoded = decoded;
					next();
				}
			});
		} else {
			// if there is no token
			// return an error
			return res.send({code: 403, message: "Must be logged in for this"})
		}
	}
}


module.exports = UsersController;