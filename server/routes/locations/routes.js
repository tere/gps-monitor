let Locations = require('./locations.js');
let doResponse = require('../../utils/common').doResponse;
let UsersController = require('../users/users');
let DevicesController = require('../devices//devices');

module.exports = (app, router) => {
    router.get("/location/:deviceID/:timePagination", UsersController.authorize, (req, res) => {
        return new Promise((resolve, reject) => {
            Locations.getDeviceLocations(req.params.deviceID, req.params.timePagination).then(locations => {
                return res.send(doResponse(locations));
            }).catch(reason => {
                console.log(reason);
                return res.send(doResponse(reason.code, reason.message));
            });
        });
    })
    router.post('/location/:deviceID', (req, res) => {
        Locations.registerLocation(req.body, req.params.deviceID).then(() => {
            return res.status(200).send();
        }).catch(() => {
            return res.status(500).send();
        })
    });
    router.post('/location/sync/:deviceID', (req, res) => {
        Locations.registerLocation(req.body, req.params.deviceID).then(() => {
            return res.status(200).send();
        }).catch(() => {
            return res.status(500).send();
        })
    });
}