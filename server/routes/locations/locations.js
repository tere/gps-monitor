let locations = require('../../models/location');


let LocationController = {
    getDeviceLocations: (deviceID, timePagination) => {
        return new Promise((resolve, reject) => {
            console.log("getDeviceLocations", deviceID, timePagination);
            locations.find({ deviceID: deviceID, time: { $gt: timePagination } }).exec().then(locations => {
                return resolve(locations);
            }).catch(reason => {
                console.log(reason);
                return reject({code: 500, message: 'Could not get locations'});
            })
        })
    },
    registerLocation: (deviceLocations, deviceID) => {
        return new Promise((resolve, reject) => {
            deviceLocations = deviceLocations.map(l => {
                return {
                    altitude: l.altitude,
                    longitude: l.longitude,
                    latitude: l.latitude,
                    accuracy: l.accuracy,
                    time: l.time,
                    deviceID: deviceID
                }
            });
            locations.insertMany(deviceLocations, (err, docs) => {
                if(err) return reject();
                return resolve();
            });
        });
    }
}


module.exports = LocationController;