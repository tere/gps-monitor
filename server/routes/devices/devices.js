let devices = require('../../models/device');
var mongoose = require('mongoose');


let DevicesController = {
    getDevice: (uuid) => {
        return new Promise((resolve, reject) => {
            devices.findOne({uuid}).exec().then(device => {
                if(device) {
                    return resolve(device);
                } 
                return reject(null);
            }).catch(reason => {
                return reject(null);
            })
        })
    },
    getUserDevice: (userID) => {
        return new Promise((resolve, reject) => {
            console.log(userID);
            devices.find({userID: mongoose.Schema.Types.ObjectId(userID)}).exec().then(devices => {
                return resolve(devices);
            }).catch(reason => {
                return reject({code: 501, message: "Could not retrieve devices"});
            })
        })
    },
    registerDevice: (uuid, userId, deviceName) => {
        return new Promise((resolve, reject) => {
            devices.create({uuid, userId, deviceName},(err, user) => {
                if(err) {
                    return reject(err);
                }
                return resolve(user);
            });
        });
    },
    renameDevice: (deviceID, newName) => {
        return new Promise((resolve, reject) => {
            console.log(deviceID, newName)
            devices.findOneAndUpdate({
                uuid: deviceID
            }, {
                $set: {
                    deviceName: newName
                },
                
            }, { new: true }, (err, doc, res) => {
                if(err) return reject({code: 501, message: 'Could not update'});
                return resolve(doc);
            })
        })
    }
}


module.exports = DevicesController;