let DevicesController = require('./devices');
let UsersController = require('../users/users');
let doResponse = require('../../utils/common').doResponse;

module.exports = (app, router) => {
    router.get("/device/all", UsersController.authorize, (req, res) => {
        console.log(req.decoded);
        DevicesController.getUserDevice(req.decoded.userID).then(devices => {
            return res.send(doResponse(devices));
        }).catch(reason => {
            return res.send(doResponse(reason.code, reason.message));
        });
    });

    router.post('/device/rename/:id', UsersController.authorize, (req, res) => {
        DevicesController.renameDevice(req.params.id, req.body.newName).then(result => {
            return res.send(doResponse(result));
        }).catch(reason => {
            return res.send(doResponse(reason.code, reason.message));
        });
    })
}