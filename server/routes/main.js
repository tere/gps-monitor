"use strict";
var express = require('express');
var router = express.Router();


module.exports = (app) => {
	require("./users/routes")(app, router);
	require("./devices/routes")(app, router);
	require("./locations/routes")(app, router);
	app.use("/", router);
}
