
'use strict';
const mongoose = require('mongoose');

const locationModel = new mongoose.Schema({
    deviceID: {
        type: String,
        required: true
    },
    latitude: {
        type: String,
        required: true
    },
    longitude: {
        type: String,
        required: true,
    },
    altitude: {
        type: String,
        required: true
    },
    accuracy: {
        type: Number,
        required: true
    },
    time: {
        type: Number,
        required: true
    }
}, {
    toJSON: {
        virtuals: true,
    },
    toObject: {
        virtuals: true
    }
});
mongoose.Schema.Types.Location = locationModel;
module.exports = mongoose.model('Location', locationModel);