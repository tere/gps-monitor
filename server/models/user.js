
'use strict';
const mongoose = require('mongoose');

const UserModel = new mongoose.Schema({
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: false
    }
}, {
    toJSON: {
        virtuals: true,
    },
    toObject: {
        virtuals: true
    }
});
mongoose.Schema.Types.User = UserModel;
module.exports = mongoose.model('User', UserModel);