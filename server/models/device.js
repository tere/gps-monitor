
'use strict';
const mongoose = require('mongoose');

const deviceModel = new mongoose.Schema({
    uuid: {
        type: String,
        required: true
    },
    userID: {
        type: mongoose.Schema.Types.ObjectId,
        required: false
    },
    deviceName: {
        type: String,
        required: true,
    },
}, {
    toJSON: {
        virtuals: true,
    },
    toObject: {
        virtuals: true
    }
});
mongoose.Schema.Types.Devices = deviceModel;
module.exports = mongoose.model('Device', deviceModel);