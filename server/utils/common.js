
const doResponse = (...args) => {
    let response = {
		errorCode: null,
		errorMessage: null,
		result: null
    }
    if(args.length === 1) {
        response.result = args[0];
    } else {
        response.errorCode = args[0];
        response.errorMessage = args[1];
    }
    return response;
}


module.exports = {
    doResponse
}