webpackJsonp([0],{

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular_util_events__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__login_login__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_settings_settings__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_background_geolocation__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular_components_alert_alert_controller__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__settings_settings__ = __webpack_require__(56);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var HomePage = (function () {
    function HomePage(navCtrl, settings, backgroundGeolocation, alertCtrl, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.settings = settings;
        this.backgroundGeolocation = backgroundGeolocation;
        this.alertCtrl = alertCtrl;
        this.events = events;
        this.backgroundGeoLocationIsRunning = false;
        this.events.subscribe('location:received', function () {
            _this.backgroundGeoLocationIsRunning = true;
        });
    }
    HomePage.prototype.stopBackgroundGeolocation = function () {
        var alert = this.alertCtrl.create({
            title: "Background Location tracking has been stoped",
            subTitle: "Location tracking will start again with the application start",
            buttons: ["Ok"]
        });
        this.backgroundGeolocation.stop().then(function () {
            alert.present();
        }).catch(function () {
            console.log("Hopefully not");
        });
    };
    HomePage.prototype.goToRegister = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_1__login_login__["a" /* LoginPage */]);
    };
    HomePage.prototype.goToSettings = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__settings_settings__["a" /* SettingsPage */]);
    };
    HomePage.prototype.registerDevice = function () {
        console.log("Device uuid: ", this.settings.getDeviceId());
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/andreiterecoasa/Documents/Personal/Projects/gps-monitor/GPSMonitorAndroid/src/pages/home/home.html"*/'<ion-header>\n	<ion-navbar>\n		<button ion-button menuToggle>\n			<ion-icon name="menu"></ion-icon>\n		</button>\n		<ion-title>Home</ion-title>\n	</ion-navbar>\n</ion-header>\n\n<ion-content padding>\n	<h2 class="content-title">GeoLocator</h2>\n	<div class="action-buttons">\n		<button ion-button full primary round (click)="goToRegister()">Register/Login</button>\n		<button ion-button full secondary round color="danger" (click)="stopBackgroundGeolocation()" *ngIf="backgroundGeoLocationIsRunning">Stop GeoLocation</button>\n		<button ion-button full secondary round color="dark" (click)="goToSettings()">Settings</button>\n	</div>\n	<div class="project-description">\n		<h5>Gandit, Proiectat, Dezvoltat si Prezentat de:</h5>\n		<p>Andreea Oltean</p>\n		<p>Andrei Cristian Terecoasa</p>\n	</div>\n</ion-content>\n'/*ion-inline-end:"/Users/andreiterecoasa/Documents/Personal/Projects/gps-monitor/GPSMonitorAndroid/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["f" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_settings_settings__["a" /* SettingsProvider */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_background_geolocation__["a" /* BackgroundGeolocation */],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular_components_alert_alert_controller__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular_util_events__["a" /* Events */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_user_user__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_loading_loading_controller__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular_components_alert_alert_controller__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__home_home__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__settings_settings__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ionic_angular_util_events__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = (function () {
    function LoginPage(navCtrl, navParams, up, LoadingController, AlertController, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.up = up;
        this.LoadingController = LoadingController;
        this.AlertController = AlertController;
        this.events = events;
        this.registerCredentials = { email: "", password: "" };
        this.loginMode = false;
        this.loadingCtrl = null;
        this.alertCtrl = null;
        this.user = up;
        this.loadingCtrl = this.LoadingController.create({
            content: 'Please wait...'
        });
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log("ionViewDidLoad LoginPage");
        this.user.getLocalUserData().then(function (data) {
            console.log(data);
        });
    };
    LoginPage.prototype.doConnect = function () {
        var _this = this;
        this.loadingCtrl.present();
        this.user.connect({ email: this.registerCredentials.email, password: this.registerCredentials.password }).then(function (data) {
            _this.events.publish('user:loggedIn');
            var alert = _this.AlertController.create({
                title: 'yay',
                subTitle: 'You have been succesfully logged in',
                buttons: [{
                        text: 'Go Home',
                        handler: function () {
                            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__home_home__["a" /* HomePage */]);
                        }
                    }, {
                        text: 'Go to Settings',
                        handler: function () {
                            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__settings_settings__["a" /* SettingsPage */]);
                        }
                    }]
            });
            _this.loadingCtrl.dismiss();
            alert.present();
        }).catch(function (reason) {
            console.log(reason);
            var alert = _this.AlertController.create({
                title: 'Could not connect',
                subTitle: 'Something bad happened',
                buttons: ['Ok']
            });
            _this.loadingCtrl.dismiss();
            alert.present();
        });
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-login",template:/*ion-inline-start:"/Users/andreiterecoasa/Documents/Personal/Projects/gps-monitor/GPSMonitorAndroid/src/pages/login/login.html"*/'<ion-content class="login-content" padding>\n	<ion-row>\n		<img class="appImage" src="/assets/imgs/navigation.png" alt="">\n	</ion-row>\n	<div class="login-box">\n		<form (ngSubmit)="false" #registerForm="ngForm">\n			<ion-row>\n				<ion-col>\n					<ion-list inset>\n						<ion-item>\n							<ion-input type="text" placeholder="Email" name="email" [(ngModel)]="registerCredentials.email" required></ion-input>\n						</ion-item>\n						<ion-item>\n							<ion-input type="password" placeholder="Password" name="password" [(ngModel)]="registerCredentials.password" required></ion-input>\n						</ion-item>\n					</ion-list>\n				</ion-col>\n			</ion-row>\n			<ion-row>\n				<ion-col class="signup-col">\n					<button ion-button class="submit-btn" id="register" full (click)="doConnect()" [disabled]="!registerForm.form.valid">Connect</button>\n				</ion-col>\n			</ion-row>\n		</form>\n	</div>\n</ion-content>\n'/*ion-inline-end:"/Users/andreiterecoasa/Documents/Personal/Projects/gps-monitor/GPSMonitorAndroid/src/pages/login/login.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_loading_loading_controller__["a" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular_components_alert_alert_controller__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_7_ionic_angular_util_events__["a" /* Events */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__settings_settings__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__constants_constants__ = __webpack_require__(113);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var UserProvider = (function () {
    // private serverURI: string = "http://10.178.116.112:9001/user";
    function UserProvider(storage, settings, http, constants) {
        this.storage = storage;
        this.settings = settings;
        this.http = http;
        this.constants = constants;
        this.user = {};
        this.serverURI = "http://localhost:9001/user";
        console.log("Hello UserProvider Provider");
        this.serverURI = this.constants.get('serverURI') + "/user";
    }
    UserProvider.prototype.getLocalUserData = function () {
        return this.storage.get("userData").then(function (data) {
            console.log(data);
        }, function (error) {
            console.log(error);
        });
    };
    UserProvider.prototype.clearLocalStorageData = function (key) {
        this.storage.remove(key);
    };
    UserProvider.prototype.connect = function (userData) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.serverURI + '/connect', {
                deviceID: _this.settings.getDeviceId() || 'browser',
                userData: userData
            }).subscribe(function (data) {
                console.log(data);
                if (data.errorCode) {
                    return reject(data.errorMessage);
                }
                Promise.all([
                    _this.settings.setUserToken(data.result.token),
                    Promise.resolve(delete data.result.token),
                    _this.settings.setUserData(data.result)
                ]).then(function () {
                    return resolve(data.result);
                });
            }, function (err) {
                return reject(err);
            });
        });
    };
    UserProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_3__settings_settings__["a" /* SettingsProvider */],
            __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_4__constants_constants__["a" /* ConstantsProvider */]])
    ], UserProvider);
    return UserProvider;
}());

//# sourceMappingURL=user.js.map

/***/ }),

/***/ 113:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConstantsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the ConstantsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var ConstantsProvider = (function () {
    function ConstantsProvider(http) {
        this.http = http;
        this.constants = {
            serverURI: 'https://gps-monitor.eu-gb.mybluemix.net'
            // serverURI: 'http://10.178.116.112:9001'
            // serverURI: 'http://localhost:9001'
            // serverURI: 'http://192.168.1.2:9001'
        };
    }
    ConstantsProvider.prototype.get = function (key) {
        return this.constants[key];
    };
    ConstantsProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClient */]])
    ], ConstantsProvider);
    return ConstantsProvider;
}());

//# sourceMappingURL=constants.js.map

/***/ }),

/***/ 125:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 125;

/***/ }),

/***/ 165:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 165;

/***/ }),

/***/ 211:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DevicesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__browser_map_browser_map__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_devices_devices__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__map_map__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular_components_alert_alert_controller__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular_platform_platform__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the DevicesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DevicesPage = (function () {
    function DevicesPage(navCtrl, navParams, nav, devices, alertCtrl, platform) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.nav = nav;
        this.devices = devices;
        this.alertCtrl = alertCtrl;
        this.platform = platform;
        this.userDevices = [];
        this.isOnDevice = false;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.isOnDevice = _this.platform.is('cordova');
        });
    }
    DevicesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DevicesPage');
        this.getUserDevices();
    };
    DevicesPage.prototype.getUserDevices = function () {
        var _this = this;
        this.devices.getAllDevices().then(function (devices) {
            _this.userDevices = devices;
        }).catch(function (err) {
            console.log(err);
        });
    };
    DevicesPage.prototype.viewOnMap = function (device) {
        this.navCtrl.push(this.isOnDevice ? __WEBPACK_IMPORTED_MODULE_4__map_map__["a" /* MapPage */] : __WEBPACK_IMPORTED_MODULE_0__browser_map_browser_map__["a" /* BrowserMapPage */], {
            device: device
        });
    };
    DevicesPage.prototype.renameDevice = function (device, i) {
        var _this = this;
        var renameAlert = this.alertCtrl.create({
            title: "Change name of: " + device.deviceName,
            subTitle: "Enter the new name",
            inputs: [
                {
                    name: 'deviceName',
                    placeholder: 'New Device Name',
                    type: 'text',
                    value: device.deviceName
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function () {
                        console.log('Cancel');
                    }
                },
                {
                    text: 'Save',
                    handler: function (data) {
                        _this.devices.renameDevice(device.uuid, data.deviceName).then(function (result) {
                            _this.userDevices[i].deviceName = data.deviceName;
                        });
                    }
                }
            ]
        });
        renameAlert.present();
    };
    DevicesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-devices',template:/*ion-inline-start:"/Users/andreiterecoasa/Documents/Personal/Projects/gps-monitor/GPSMonitorAndroid/src/pages/devices/devices.html"*/'<!--\n  Generated template for the DevicesPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n		<ion-navbar>\n			<button ion-button menuToggle>\n				<ion-icon name="menu"></ion-icon>\n			</button>\n			<ion-title>Device</ion-title>\n		</ion-navbar>\n	</ion-header>\n\n\n<ion-content padding>\n	<ion-list>\n		<ion-item *ngFor="let device of userDevices; let i = index">\n			<ion-item-sliding>\n				<ion-item>\n					<h2>{{device.deviceName}}</h2>\n					<p><b>ID:</b> {{device.uuid}}</p>\n				</ion-item>\n				<ion-item-options side="left">\n					<button ion-button color="primary" (click)="renameDevice(device, i)">\n						<ion-icon name="text"></ion-icon>\n						Rename\n					</button>\n					<button ion-button color="secondary" (click)="viewOnMap(device)">\n						<ion-icon name="map"></ion-icon>\n						View On Map\n					</button>\n				</ion-item-options>\n				<ion-item-options side="right">\n					<button ion-button color="primary">\n						<ion-icon name="mail"></ion-icon>\n						Destroy\n					</button>\n				</ion-item-options>\n			</ion-item-sliding>\n		</ion-item>\n	</ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/andreiterecoasa/Documents/Personal/Projects/gps-monitor/GPSMonitorAndroid/src/pages/devices/devices.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_devices_devices__["a" /* DevicesProvider */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular_components_alert_alert_controller__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular_platform_platform__["a" /* Platform */]])
    ], DevicesPage);
    return DevicesPage;
}());

//# sourceMappingURL=devices.js.map

/***/ }),

/***/ 212:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BrowserMapPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular_components_alert_alert_controller__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_devices_devices__ = __webpack_require__(57);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var BrowserMapPage = (function () {
    function BrowserMapPage(navCtrl, navParams, devices, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.devices = devices;
        this.alertCtrl = alertCtrl;
        this.locations = [];
        this.timePagination = 0;
        this.interval = null;
        this.refreshInterval = 2000;
        this.device = navParams.get("device");
        console.log(this.device);
    }
    BrowserMapPage.prototype.initMap = function () {
        console.log("init map");
    };
    BrowserMapPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BrowserMapPage');
        this.loadMap();
    };
    BrowserMapPage.prototype.startAutoRefresh = function () {
        var _this = this;
        if (this.interval) {
            clearInterval(this.interval);
            this.interval = null;
        }
        this.interval = setInterval(function () {
            _this.getDeviceLocations();
        }, this.refreshInterval);
    };
    BrowserMapPage.prototype.loadMap = function () {
        console.log("load map");
        var latLng = new google.maps.LatLng(-34.9290, 138.6010);
        var mapOptions = {
            center: latLng,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
        this.getDeviceLocations();
    };
    BrowserMapPage.prototype.changeRefreshInterval = function () {
        var _this = this;
        var changeRefreshIntervalAlert = this.alertCtrl.create({
            title: "Change refresh interval",
            subTitle: "Interval in ms for refreshing rate",
            inputs: [
                {
                    name: "newRefreshInterval",
                    value: this.refreshInterval,
                    type: "text",
                }
            ],
            buttons: [
                {
                    text: "Save",
                    handler: function (data) {
                        _this.refreshInterval = data.newRefreshInterval;
                        _this.startAutoRefresh();
                    }
                }
            ]
        });
        console.log(changeRefreshIntervalAlert);
        changeRefreshIntervalAlert.present();
    };
    BrowserMapPage.prototype.getDeviceLocations = function () {
        var _this = this;
        this.devices.getDeviceLocation(this.device.uuid, this.timePagination).then(function (locations) {
            console.log(locations, _this.locations);
            if (locations.length == 0)
                return;
            _this.locations = _this.locations.concat(locations);
            new google.maps.Marker({
                position: { lat: parseFloat(_this.locations[0].latitude), lng: parseFloat(_this.locations[0].longitude) },
                map: _this.map
            });
            new google.maps.Marker({
                position: { lat: parseFloat(_this.locations[_this.locations.length - 1].latitude), lng: parseFloat(_this.locations[_this.locations.length - 1].longitude) },
                map: _this.map
            });
            _this.timePagination = _this.locations[_this.locations.length - 1].time;
            _this.map.setCenter({ lat: parseFloat(_this.locations[_this.locations.length - 1].latitude), lng: parseFloat(_this.locations[_this.locations.length - 1].longitude) });
            _this.tracePolyline();
        });
    };
    BrowserMapPage.prototype.tracePolyline = function () {
        var path = this.locations.map(function (l) {
            return {
                lat: parseFloat(l.latitude),
                lng: parseFloat(l.longitude)
            };
        });
        console.log(path);
        new google.maps.Polyline({
            path: path,
            geodesic: true,
            strokeColor: '#FF0000',
            strokeOpacity: 1.0,
            strokeWeight: 2
        }).setMap(this.map);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_8" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__angular_core__["t" /* ElementRef */])
    ], BrowserMapPage.prototype, "mapElement", void 0);
    BrowserMapPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-browser-map',template:/*ion-inline-start:"/Users/andreiterecoasa/Documents/Personal/Projects/gps-monitor/GPSMonitorAndroid/src/pages/browser-map/browser-map.html"*/'<ion-header>\n\n	<ion-navbar>\n		<ion-title>Trace for {{device.deviceName}}</ion-title>\n	</ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n\n\n	<button ion-button secondary color="primary" (click)="changeRefreshInterval()">Refresh Interval</button>\n	<div #map id="map">\n	</div>\n</ion-content>\n'/*ion-inline-end:"/Users/andreiterecoasa/Documents/Personal/Projects/gps-monitor/GPSMonitorAndroid/src/pages/browser-map/browser-map.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_devices_devices__["a" /* DevicesProvider */],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular_components_alert_alert_controller__["a" /* AlertController */]])
    ], BrowserMapPage);
    return BrowserMapPage;
}());

//# sourceMappingURL=browser-map.js.map

/***/ }),

/***/ 213:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_devices_devices__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular_components_alert_alert_controller__ = __webpack_require__(29);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the MapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MapPage = (function () {
    function MapPage(navCtrl, navParams, googleMaps, toastCtrl, devices, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.googleMaps = googleMaps;
        this.toastCtrl = toastCtrl;
        this.devices = devices;
        this.alertCtrl = alertCtrl;
        this.mapReady = false;
        this.timePagination = 0;
        this.refreshInterval = 2000;
        this.interval = null;
        this.device = navParams.get("device");
        console.log(this.device);
    }
    MapPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MapPage');
        this.loadMap();
        this.startAutoRefresh();
    };
    MapPage.prototype.startAutoRefresh = function () {
        var _this = this;
        if (this.interval) {
            clearInterval(this.interval);
            this.interval = null;
        }
        this.interval = setInterval(function () {
            _this.getDeviceLocations();
        }, this.refreshInterval);
    };
    MapPage.prototype.changeRefreshInterval = function () {
        var _this = this;
        var changeRefreshIntervalAlert = this.alertCtrl.create({
            title: "Change refresh interval",
            subTitle: "Interval in ms for refreshing rate",
            inputs: [
                {
                    name: "newRefreshInterval",
                    value: this.refreshInterval,
                    type: "text",
                }
            ],
            buttons: [
                {
                    text: "Save",
                    handler: function (data) {
                        _this.refreshInterval = data.newRefreshInterval;
                        _this.startAutoRefresh();
                    }
                }
            ]
        });
        console.log(changeRefreshIntervalAlert);
        changeRefreshIntervalAlert.present();
    };
    MapPage.prototype.loadMap = function () {
        var _this = this;
        // Create a map after the view is loaded.
        // (platform is already ready in app.component.ts)
        this.map = __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["a" /* GoogleMaps */].create('map_canvas', {
            camera: {
                target: {
                    lat: 45.23574366532972,
                    lng: 25.64144643590804
                },
                zoom: 13,
                tilt: 30
            }
        });
        // Wait the maps plugin is ready until the MAP_READY event
        this.map.one(__WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["b" /* GoogleMapsEvent */].MAP_READY).then(function () {
            _this.mapReady = true;
        });
    };
    MapPage.prototype.getDeviceLocations = function () {
        var _this = this;
        this.devices.getDeviceLocation(this.device.uuid, this.timePagination).then(function (locations) {
            console.log(locations);
            _this.locations = locations;
            _this.map.addMarker({
                position: { lat: _this.locations[0].latitude, lng: _this.locations[0].longitude },
                title: _this.locations[0].altitude
            });
            _this.map.addMarker({
                position: { lat: _this.locations[_this.locations.length - 1].latitude, lng: _this.locations[_this.locations.length - 1].longitude },
                title: _this.locations[_this.locations.length - 1].altitude
            });
            _this.timePagination = _this.locations[_this.locations.length - 1].time;
            _this.tracePolyline();
        });
    };
    MapPage.prototype.tracePolyline = function () {
        this.map.addPolyline({
            points: this.locations.map(function (l) {
                return {
                    lat: l.latitude,
                    lng: l.longitude
                };
            }),
            width: 2
        });
    };
    MapPage.prototype.onButtonClick = function () {
        if (!this.mapReady) {
            this.showToast('map is not ready yet. Please try again.');
            return;
        }
        this.map.clear();
        // Get the location of you
        // this.map.getMyLocation()
        // 	.then((location: MyLocation) => {
        // 		console.log(JSON.stringify(location, null, 2));
        // 		// Move the map camera to the location with animation
        // 		return this.map.animateCamera({
        // 			target: location.latLng,
        // 			zoom: 17,
        // 			tilt: 30
        // 		}).then(() => {
        // 			// add a marker
        // 			return this.map.addMarker({
        // 				title: '@ionic-native/google-maps plugin!',
        // 				snippet: 'This plugin is awesome!',
        // 				position: location.latLng,
        // 				animation: GoogleMapsAnimation.BOUNCE
        // 			});
        // 		})
        // 	}).then((marker: Marker) => {
        // 		// show the infoWindow
        // 		marker.showInfoWindow();
        // 		// If clicked it, display the alert
        // 		marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
        // 			this.showToast('clicked!');
        // 		});
        // 	});
    };
    MapPage.prototype.showToast = function (message) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: 2000,
            position: 'middle'
        });
        toast.present(toast);
    };
    MapPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-map',template:/*ion-inline-start:"/Users/andreiterecoasa/Documents/Personal/Projects/gps-monitor/GPSMonitorAndroid/src/pages/map/map.html"*/'<!--\n  Generated template for the MapPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n	<ion-navbar>\n		<ion-title>Trace for {{device.deviceName}}</ion-title>\n	</ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n\n\n	<div id="map_canvas">\n	<button ion-button secondary color="primary" (click)="changeRefreshInterval()">Refresh Interval</button>\n	</div>\n</ion-content>\n'/*ion-inline-end:"/Users/andreiterecoasa/Documents/Personal/Projects/gps-monitor/GPSMonitorAndroid/src/pages/map/map.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["a" /* GoogleMaps */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_devices_devices__["a" /* DevicesProvider */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular_components_alert_alert_controller__["a" /* AlertController */]])
    ], MapPage);
    return MapPage;
}());

//# sourceMappingURL=map.js.map

/***/ }),

/***/ 215:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(237);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 237:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_background_geolocation__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_background_mode__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_device__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_component__ = __webpack_require__(286);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_home_home__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_list_list__ = __webpack_require__(295);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_login_login__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_devices_devices__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_map_map__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_status_bar__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_splash_screen__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__providers_user_user__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_storage__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__providers_settings_settings__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_settings_settings__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__angular_common_http__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__providers_devices_devices__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__providers_http_interceptor__ = __webpack_require__(296);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__providers_constants_constants__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_google_maps__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_browser_map_browser_map__ = __webpack_require__(212);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


























var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */], __WEBPACK_IMPORTED_MODULE_8__pages_home_home__["a" /* HomePage */], __WEBPACK_IMPORTED_MODULE_9__pages_list_list__["a" /* ListPage */], __WEBPACK_IMPORTED_MODULE_10__pages_login_login__["a" /* LoginPage */], __WEBPACK_IMPORTED_MODULE_18__pages_settings_settings__["a" /* SettingsPage */], __WEBPACK_IMPORTED_MODULE_11__pages_devices_devices__["a" /* DevicesPage */], __WEBPACK_IMPORTED_MODULE_12__pages_map_map__["a" /* MapPage */], __WEBPACK_IMPORTED_MODULE_24__pages_browser_map_browser_map__["a" /* BrowserMapPage */]],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */], {}, {
                    links: []
                }),
                __WEBPACK_IMPORTED_MODULE_16__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_19__angular_common_http__["c" /* HttpClientModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [__WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */], __WEBPACK_IMPORTED_MODULE_8__pages_home_home__["a" /* HomePage */], __WEBPACK_IMPORTED_MODULE_9__pages_list_list__["a" /* ListPage */], __WEBPACK_IMPORTED_MODULE_10__pages_login_login__["a" /* LoginPage */], __WEBPACK_IMPORTED_MODULE_18__pages_settings_settings__["a" /* SettingsPage */], __WEBPACK_IMPORTED_MODULE_11__pages_devices_devices__["a" /* DevicesPage */], __WEBPACK_IMPORTED_MODULE_12__pages_map_map__["a" /* MapPage */], __WEBPACK_IMPORTED_MODULE_24__pages_browser_map_browser_map__["a" /* BrowserMapPage */]],
            providers: [
                {
                    provide: __WEBPACK_IMPORTED_MODULE_19__angular_common_http__["a" /* HTTP_INTERCEPTORS */],
                    useClass: __WEBPACK_IMPORTED_MODULE_21__providers_http_interceptor__["a" /* ServerAuthInterceptor */],
                    multi: true
                },
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_14__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_15__providers_user_user__["a" /* UserProvider */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_background_geolocation__["a" /* BackgroundGeolocation */],
                __WEBPACK_IMPORTED_MODULE_17__providers_settings_settings__["a" /* SettingsProvider */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_device__["a" /* Device */],
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_background_mode__["a" /* BackgroundMode */],
                __WEBPACK_IMPORTED_MODULE_20__providers_devices_devices__["a" /* DevicesProvider */],
                __WEBPACK_IMPORTED_MODULE_22__providers_constants_constants__["a" /* ConstantsProvider */],
                __WEBPACK_IMPORTED_MODULE_23__ionic_native_google_maps__["a" /* GoogleMaps */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 286:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_home_home__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_login_login__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_user_user__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_background_geolocation__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_settings_settings__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_devices_devices__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_settings_settings__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_background_mode__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_ionic_angular_util_events__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














var MyApp = (function () {
    function MyApp(backgroundGeolocation, platform, statusBar, splashScreen, user, geo, settings, backgroundMode, events) {
        var _this = this;
        this.backgroundGeolocation = backgroundGeolocation;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.user = user;
        this.geo = geo;
        this.settings = settings;
        this.backgroundMode = backgroundMode;
        this.events = events;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */];
        this.isOnDevice = false;
        this.loginPage = { title: 'Login', component: __WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* LoginPage */] };
        this.isLoggedIn = false;
        this.homePage = { title: 'Home', component: __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */] };
        this.events.subscribe('user:loggedIn', function () {
            console.log("user loggedin");
            _this.isLoggedIn = true;
            console.log(_this.loggedInPages);
        });
        this.events.subscribe('user:loggedout', function () {
            console.log("user loggedout");
            _this.isLoggedIn = false;
            _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */]);
            if (_this.isOnDevice) {
                _this.backgroundGeolocation.stop();
            }
        });
        this.pages = [
            { title: 'Home', component: __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */] }
        ];
        this.loggedInPages = [
            { title: 'Settings', component: __WEBPACK_IMPORTED_MODULE_9__pages_settings_settings__["a" /* SettingsPage */] },
            { title: 'Devices', component: __WEBPACK_IMPORTED_MODULE_10__pages_devices_devices__["a" /* DevicesPage */] },
        ];
        this.initializeApp();
    }
    MyApp.prototype.ionViewDidLoad = function () {
    };
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        // used for an example of ngFor and navigation
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
            _this.isOnDevice = _this.platform.is('cordova');
            if (_this.isOnDevice) {
                _this.startGeoLocation();
            }
        });
    };
    MyApp.prototype.cleanLocalStorage = function () {
        this.user.clearLocalStorageData("userData");
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    MyApp.prototype.logout = function () {
        this.settings.removeSingle('userToken');
        this.isLoggedIn = false;
        this.events.publish('user:loggedout');
    };
    MyApp.prototype.isGeoLocationRunning = function () {
        if (!this.isOnDevice)
            return;
    };
    MyApp.prototype.startGeoLocation = function () {
        var _this = this;
        this.settings.getAppSettings().then(function (config) {
            if (!config) {
                return;
            }
            _this.backgroundGeolocation.configure(config)
                .subscribe(function (location) {
                console.log("Got location: ", location);
                _this.events.publish('location:received');
                // IMPORTANT:  You must execute the finish method here to inform the native plugin that you're finished,
                // and the background-task may be completed.  You must do this regardless if your HTTP request is successful or not.
                // IF YOU DON'T, ios will CRASH YOUR APP for spending too much time in the background.
                // this.backgroundGeolocation.finish(); // FOR IOS ONLY
            });
            // start recording location
            _this.backgroundGeolocation.start();
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/andreiterecoasa/Documents/Personal/Projects/gps-monitor/GPSMonitorAndroid/src/app/app.html"*/'<ion-menu [content]="content">\n	<ion-header>\n		<ion-toolbar>\n			<ion-title>Menu</ion-title>\n		</ion-toolbar>\n	</ion-header>\n\n	<ion-content>\n		<ion-list *ngIf="isLoggedIn">\n			<button menuClose ion-item (click)="openPage(homePage)">\n				{{homePage.title}}\n			</button>\n			<button menuClose ion-item *ngFor="let p of loggedInPages" (click)="openPage(p)">\n				{{p.title}}\n			</button>\n		</ion-list>\n		<ion-list>\n			<button menuClose ion-item (click)="openPage(loginPage)" *ngIf="!isLoggedIn">\n				Login\n			</button>\n			<button menuClose ion-item (click)="logout()" *ngIf="isLoggedIn">\n				Logout\n			</button>\n		</ion-list>\n	</ion-content>\n\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>\n'/*ion-inline-end:"/Users/andreiterecoasa/Documents/Personal/Projects/gps-monitor/GPSMonitorAndroid/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_8__ionic_native_background_geolocation__["a" /* BackgroundGeolocation */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_7__providers_user_user__["a" /* UserProvider */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_11__providers_settings_settings__["a" /* SettingsProvider */],
            __WEBPACK_IMPORTED_MODULE_12__ionic_native_background_mode__["a" /* BackgroundMode */],
            __WEBPACK_IMPORTED_MODULE_13_ionic_angular_util_events__["a" /* Events */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 295:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ListPage = (function () {
    function ListPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        // If we navigated to this page, we will have an item available as a nav param
        this.selectedItem = navParams.get('item');
        // Let's populate this page with some filler content for funzies
        this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
            'american-football', 'boat', 'bluetooth', 'build'];
        this.items = [];
        for (var i = 1; i < 11; i++) {
            this.items.push({
                title: 'Item ' + i,
                note: 'This is item #' + i,
                icon: this.icons[Math.floor(Math.random() * this.icons.length)]
            });
        }
    }
    ListPage_1 = ListPage;
    ListPage.prototype.itemTapped = function (event, item) {
        // That's right, we're pushing to ourselves!
        this.navCtrl.push(ListPage_1, {
            item: item
        });
    };
    ListPage = ListPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-list',template:/*ion-inline-start:"/Users/andreiterecoasa/Documents/Personal/Projects/gps-monitor/GPSMonitorAndroid/src/pages/list/list.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>List</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <button ion-item *ngFor="let item of items" (click)="itemTapped($event, item)">\n      <ion-icon [name]="item.icon" item-start></ion-icon>\n      {{item.title}}\n      <div class="item-note" item-end>\n        {{item.note}}\n      </div>\n    </button>\n  </ion-list>\n  <div *ngIf="selectedItem" padding>\n    You navigated here from <b>{{selectedItem.title}}</b>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/andreiterecoasa/Documents/Personal/Projects/gps-monitor/GPSMonitorAndroid/src/pages/list/list.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], ListPage);
    return ListPage;
    var ListPage_1;
}());

//# sourceMappingURL=list.js.map

/***/ }),

/***/ 296:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServerAuthInterceptor; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_settings_settings__ = __webpack_require__(35);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};


var ServerAuthInterceptor = (function () {
    function ServerAuthInterceptor(settings) {
        var _this = this;
        this.settings = settings;
        this.token = 'notloggedin';
        this.settings.getUserToken().then(function (token) {
            if (token) {
                _this.token = token;
            }
        });
    }
    ServerAuthInterceptor.prototype.getToken = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.settings.getUserToken()];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ServerAuthInterceptor.prototype.intercept = function (req, next) {
        var authReq = req.clone({
            headers: req.headers.set('Authorization', this.settings.authHeader)
        });
        return next.handle(authReq);
    };
    ServerAuthInterceptor = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__providers_settings_settings__["a" /* SettingsProvider */]])
    ], ServerAuthInterceptor);
    return ServerAuthInterceptor;
}());

//# sourceMappingURL=http.interceptor.js.map

/***/ }),

/***/ 35:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_device__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular_util_events__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the SettingsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var SettingsProvider = (function () {
    function SettingsProvider(storage, device, events) {
        var _this = this;
        this.storage = storage;
        this.device = device;
        this.events = events;
        this.authHeader = "notLoggedIn";
        console.log("Hello SettingsProvider Provider");
        this.getUserToken().then(function (token) {
            _this.events.publish('user:loggedIn');
            _this.authHeader = token.toString();
        });
    }
    SettingsProvider.prototype.setSingle = function (key, value, isJson) {
        return this.storage.set(key, isJson ? JSON.stringify(value) : value);
    };
    SettingsProvider.prototype.getSingle = function (key, isJson) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get(key).then(function (data) {
                console.log("Get Single [" + key + "] = ", (isJson ? JSON.parse(data) : data));
                return resolve(data ? (isJson ? JSON.parse(data) : data) : (isJson ? null : ''));
            }).catch(function (reason) {
                return reject(reason);
            });
        });
    };
    SettingsProvider.prototype.removeSingle = function (key) {
        return this.storage.remove('userToken');
    };
    SettingsProvider.prototype.getDeviceId = function () {
        return this.device.uuid;
    };
    SettingsProvider.prototype.getAppSettings = function () {
        return this.getSingle("appSettings", true);
    };
    SettingsProvider.prototype.setAppSettings = function (settings) {
        return this.setSingle("appSettings", settings, true);
    };
    SettingsProvider.prototype.setUserToken = function (token) {
        this.authHeader = token;
        return this.setSingle('userToken', token, false);
    };
    SettingsProvider.prototype.getUserToken = function () {
        return this.getSingle('userToken', false);
    };
    SettingsProvider.prototype.setUserData = function (data) {
        return this.setSingle('userData', data, true);
    };
    SettingsProvider.prototype.getUserData = function () {
        return this.getSingle('userData', true);
    };
    SettingsProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_device__["a" /* Device */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular_util_events__["a" /* Events */]])
    ], SettingsProvider);
    return SettingsProvider;
}());

//# sourceMappingURL=settings.js.map

/***/ }),

/***/ 56:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_settings_settings__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_background_geolocation__ = __webpack_require__(55);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SettingsPage = (function () {
    function SettingsPage(alertCtrl, navCtrl, navParams, settings, backgroundGeolocation) {
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.settings = settings;
        this.backgroundGeolocation = backgroundGeolocation;
        this.appSettings = {
            desiredAccuracy: 100,
            stationaryRadius: 50,
            distanceFilter: 50,
            debug: false,
            startOnBoot: false,
            stopOnTerminate: false,
            interval: 10000,
            url: "https://gps-monitor.eu-gb.mybluemix.net/location/" + this.settings.getDeviceId(),
            syncUrl: "https://gps-monitor.eu-gb.mybluemix.net/location/sync/" + this.settings.getDeviceId(),
            syncThreshold: 100,
            postTemplate: ['@latitude', '@longitude', 'foo', 'bar']
            // postTemplate: {
            // 	latitude: "@latitude",
            // 	longitude: "@longitude",
            // 	accuracy: "@accuracy",
            // 	deviceID: this.settings.getDeviceId(),
            // 	time: "@time"
            // }
        };
    }
    SettingsPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad SettingsPage');
        this.settings.getAppSettings().then(function (data) {
            if (data != null) {
                _this.appSettings = data;
            }
        });
    };
    SettingsPage.prototype.saveSettings = function () {
        var _this = this;
        this.settings.setAppSettings(this.appSettings).then(function () {
            var alert = _this.alertCtrl.create({
                title: 'Settings Saved',
                subTitle: 'Settings were saved. Please restart app for them to take effect',
                buttons: ['Ok']
            });
            alert.present();
            _this.backgroundGeolocation.stop();
        });
    };
    SettingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-settings',template:/*ion-inline-start:"/Users/andreiterecoasa/Documents/Personal/Projects/gps-monitor/GPSMonitorAndroid/src/pages/settings/settings.html"*/'<!--\n  Generated template for the SettingsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n	<ion-navbar>\n		<button ion-button menuToggle>\n			<ion-icon name="menu"></ion-icon>\n		</button>\n		<ion-title>Home</ion-title>\n	</ion-navbar>\n</ion-header>\n\n\n\n<ion-content padding>\n	<p>Settings For geolocator</p>\n	<ion-list>\n		<ion-item>\n			<ion-label>Accuracy</ion-label>\n			<ion-select [(ngModel)]="appSettings.desiredAccuracy">\n				<ion-option value="10">10</ion-option>\n				<ion-option value="100">100</ion-option>\n				<ion-option value="1000">1000</ion-option>\n			</ion-select>\n		</ion-item>\n		<ion-item>\n			<ion-label color="primary">Stationary Radius (m)</ion-label>\n			<ion-input type="number" placeholder="50" [(ngModel)]="appSettings.stationaryRadius"></ion-input>\n		</ion-item>\n		<ion-item>\n			<ion-label color="primary">Distance Filter (m)</ion-label>\n			<ion-input type="number" placeholder="50" [(ngModel)]="appSettings.distanceFilter"></ion-input>\n		</ion-item>\n		<ion-item>\n			<ion-label>Start On Boot</ion-label>\n			<ion-select [(ngModel)]="appSettings.startOnBoot">\n				<ion-option value="true">Yes</ion-option>\n				<ion-option value="false">No</ion-option>\n			</ion-select>\n		</ion-item>\n		<ion-item>\n			<ion-label>Stop On Terminate</ion-label>\n			<ion-select [(ngModel)]="appSettings.stopOnTerminate">\n				<ion-option value="true">Yes</ion-option>\n				<ion-option value="false">No</ion-option>\n			</ion-select>\n		</ion-item>\n		<ion-item>\n			<ion-label color="primary">Acquire Interval (ms)</ion-label>\n			<ion-input type="number" placeholder="50" [(ngModel)]="appSettings.interval"></ion-input>\n		</ion-item>\n	</ion-list>\n	<button ion-button full color="primary" round (click)="saveSettings()">Save</button>\n</ion-content>\n'/*ion-inline-end:"/Users/andreiterecoasa/Documents/Personal/Projects/gps-monitor/GPSMonitorAndroid/src/pages/settings/settings.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_settings_settings__["a" /* SettingsProvider */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_background_geolocation__["a" /* BackgroundGeolocation */]])
    ], SettingsPage);
    return SettingsPage;
}());

//# sourceMappingURL=settings.js.map

/***/ }),

/***/ 57:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DevicesProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__constants_constants__ = __webpack_require__(113);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DevicesProvider = (function () {
    // private serverURI: string = "http://10.178.116.112:9001/user";
    function DevicesProvider(http, constants) {
        this.http = http;
        this.constants = constants;
        this.serverURIDevice = "http://localhost:9001/device";
        this.serverURILocation = "http://localhost:9001/device";
        console.log('Hello DevicesProvider Provider');
        this.serverURIDevice = this.constants.get('serverURI') + "/device";
        this.serverURILocation = this.constants.get('serverURI') + "/location";
    }
    DevicesProvider.prototype.getAllDevices = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(_this.serverURIDevice + "/all").subscribe(function (data) {
                if (data.errorCode) {
                    return reject(data.errorMessage);
                }
                return resolve(data.result);
            }, function (err) {
                console.log(err);
            });
        });
    };
    DevicesProvider.prototype.getDeviceLocation = function (device, timePagination) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(_this.serverURILocation + "/" + device + "/" + timePagination).subscribe(function (data) {
                if (data.errorCode) {
                    return reject(data.errorMessage);
                }
                return resolve(data.result);
            }, function (err) {
                console.log(err);
            });
        });
    };
    DevicesProvider.prototype.renameDevice = function (deviceId, newName) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.serverURIDevice + "/rename/" + deviceId, {
                newName: newName
            }).subscribe(function (data) {
                if (data.errorCode) {
                    return reject(data.errorMessage);
                }
                return resolve(data.result);
            }, function (err) {
                console.log(err);
            });
        });
    };
    DevicesProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_2__constants_constants__["a" /* ConstantsProvider */]])
    ], DevicesProvider);
    return DevicesProvider;
}());

//# sourceMappingURL=devices.js.map

/***/ })

},[215]);
//# sourceMappingURL=main.js.map