import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Geolocation } from '@ionic-native/geolocation';
import { HomePage } from '../pages/home/home';
import { MapPage } from '../pages/map/map';
import { ListPage } from '../pages/list/list';
import { LoginPage } from '../pages/login/login';
import { UserProvider } from '../providers/user/user';
import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation';
import { SettingsPage } from '../pages/settings/settings';
import { DevicesPage } from '../pages/devices/devices';
import { SettingsProvider } from '../providers/settings/settings';
import { BackgroundMode } from '@ionic-native/background-mode';
import { Events } from 'ionic-angular/util/events';

@Component({
	templateUrl: 'app.html'
})
export class MyApp {
	@ViewChild(Nav) nav: Nav;

	rootPage: any = HomePage;
	isOnDevice: boolean = false;
	pages: Array<{ title: string, component: any }>;
	loginPage: { title: string, component: any } = { title: 'Login', component: LoginPage };
	loggedInPages: Array<{ title: string, component: any }>;
	isLoggedIn: boolean = false;
	homePage: { title: string, component: any } = { title: 'Home', component: HomePage };

	constructor(
		private backgroundGeolocation: BackgroundGeolocation,
		public platform: Platform,
		public statusBar: StatusBar,
		public splashScreen: SplashScreen,
		public user: UserProvider,
		public geo: Geolocation,
		public settings: SettingsProvider,
		public backgroundMode: BackgroundMode,
		public events: Events
	) {
		this.events.subscribe('user:loggedIn', () => {
			console.log("user loggedin");
			this.isLoggedIn = true;
			console.log(this.loggedInPages);
		});
		this.events.subscribe('user:loggedout', () => {
			console.log("user loggedout");
			this.isLoggedIn = false;
			this.nav.setRoot(HomePage);
			if(this.isOnDevice) {
				this.backgroundGeolocation.stop();
			}
		});
		this.pages = [
			{ title: 'Home', component: HomePage }
		];

		this.loggedInPages = [
			{ title: 'Settings', component: SettingsPage },
			{ title: 'Devices', component: DevicesPage },
		]
		this.initializeApp();
	}
	ionViewDidLoad() {

	}
	initializeApp() {
		// used for an example of ngFor and navigation
		this.platform.ready().then(() => {
			// Okay, so the platform is ready and our plugins are available.
			// Here you can do any higher level native things you might need.
			this.statusBar.styleDefault();
			this.splashScreen.hide();
			this.isOnDevice = this.platform.is('cordova');
			if(this.isOnDevice) {
				this.startGeoLocation();
			}
		});
	}
	cleanLocalStorage() {
		this.user.clearLocalStorageData("userData");
	}
	openPage(page) {
		// Reset the content nav to have just this page
		// we wouldn't want the back button to show in this scenario
		this.nav.setRoot(page.component);
	}
	logout() {
		this.settings.removeSingle('userToken');
		this.isLoggedIn = false;
		this.events.publish('user:loggedout');
	}

	isGeoLocationRunning() {
		if (!this.isOnDevice) return;
	}

	startGeoLocation() {
		this.settings.getAppSettings().then(config => {
			if (!config) {
				return;
			}
			this.backgroundGeolocation.configure(config)
				.subscribe((location: BackgroundGeolocationResponse) => {

					console.log(`Got location: `, location);
					this.events.publish('location:received');
					// IMPORTANT:  You must execute the finish method here to inform the native plugin that you're finished,
					// and the background-task may be completed.  You must do this regardless if your HTTP request is successful or not.
					// IF YOU DON'T, ios will CRASH YOUR APP for spending too much time in the background.
					// this.backgroundGeolocation.finish(); // FOR IOS ONLY

				});

			// start recording location
			this.backgroundGeolocation.start();
		})
	}

}
