import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { Geolocation } from '@ionic-native/geolocation';
import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation';
import { BackgroundMode } from '@ionic-native/background-mode';

import { Device } from '@ionic-native/device';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { LoginPage } from '../pages/login/login';
import { DevicesPage } from '../pages/devices/devices';
import { MapPage } from '../pages/map/map';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { UserProvider } from '../providers/user/user';
import { IonicStorageModule } from "@ionic/storage";
import { SettingsProvider } from '../providers/settings/settings';
import { SettingsPage } from '../pages/settings/settings';
import { HttpClientModule } from '@angular/common/http';
import { DevicesProvider } from '../providers/devices/devices';

import { ServerAuthInterceptor } from '../providers/http.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ConstantsProvider } from '../providers/constants/constants';
import { GoogleMaps } from '@ionic-native/google-maps';
import { BrowserMapPage } from '../pages/browser-map/browser-map';

@NgModule({
	declarations: [MyApp, HomePage, ListPage, LoginPage, SettingsPage, DevicesPage, MapPage, BrowserMapPage],
	imports: [
		BrowserModule,
		IonicModule.forRoot(MyApp),
		IonicStorageModule.forRoot(),
		HttpClientModule
	],
	bootstrap: [IonicApp],
	entryComponents: [MyApp, HomePage, ListPage, LoginPage, SettingsPage, DevicesPage, MapPage, BrowserMapPage],
	providers: [
		{
			provide: HTTP_INTERCEPTORS,
			useClass: ServerAuthInterceptor,
			multi: true
		},
		StatusBar,
		SplashScreen,
		{ provide: ErrorHandler, useClass: IonicErrorHandler },
		UserProvider,
		Geolocation,
		BackgroundGeolocation,
		SettingsProvider,
		Device,
		BackgroundMode,
		DevicesProvider,
		ConstantsProvider,
		GoogleMaps
	]
})
export class AppModule { }
