import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from "@ionic/storage";
import { Device } from '@ionic-native/device';
import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation';
import { Events } from 'ionic-angular/util/events';

/*
  Generated class for the SettingsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SettingsProvider {
	public authHeader: string = "notLoggedIn";
	constructor(private storage: Storage, private device: Device, private events: Events) {
		console.log("Hello SettingsProvider Provider");
		this.getUserToken().then(token => {
			this.events.publish('user:loggedIn');
			this.authHeader = token.toString();
		})
    }

	setSingle(key: string, value: string, isJson: boolean) {
		return this.storage.set(key, isJson ? JSON.stringify(value) : value);
	}
	getSingle(key: string, isJson: boolean) {
		return new Promise((resolve, reject) => {
			this.storage.get(key).then(data => {
				console.log(`Get Single [${key}] = `, (isJson ? JSON.parse(data) : data));
				return resolve(data ? (isJson ? JSON.parse(data) : data) : (isJson ? null : ''));
			}).catch(reason => {
				return reject(reason);
			});
		});
	}
	removeSingle(key: string) {
		return this.storage.remove('userToken');
	}

	getDeviceId() {
		return this.device.uuid;
	}

	getAppSettings() : Promise<BackgroundGeolocationConfig>{
		return <Promise<BackgroundGeolocationConfig>> this.getSingle("appSettings", true);
	}

	setAppSettings(settings: any) {
		return this.setSingle("appSettings", settings, true);
	}

	setUserToken(token: string) {
		this.authHeader = token;
		return this.setSingle('userToken', token, false);
	}

	getUserToken() {
		return this.getSingle('userToken', false);
	}

	setUserData(data: any) {
		return this.setSingle('userData', data, true);
	}

	getUserData() {
		return this.getSingle('userData', true);
	}
}
