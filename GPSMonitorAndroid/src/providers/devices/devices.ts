import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConstantsProvider } from '../constants/constants';

/*
  Generated class for the DevicesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

interface APIResponse {
	errorCode: number,
	errorMessage: string,
	result: any
}

@Injectable()
export class DevicesProvider {
	private serverURIDevice: string = "http://localhost:9001/device";
	private serverURILocation: string = "http://localhost:9001/device";
	// private serverURI: string = "http://10.178.116.112:9001/user";
	constructor(
		public http: HttpClient,
		public constants: ConstantsProvider
	) {
		console.log('Hello DevicesProvider Provider');
		this.serverURIDevice = this.constants.get('serverURI') + "/device";
		this.serverURILocation = this.constants.get('serverURI') + "/location";
	}

	getAllDevices() {
		return new Promise((resolve, reject) => {
			this.http.get<APIResponse>(this.serverURIDevice + "/all").subscribe(
				data => {
					if(data.errorCode) {
						return reject(data.errorMessage);
					}
					return resolve(data.result);
				},
				err => {
					console.log(err);
				}
			)
		})
	}

	getDeviceLocation(device: any, timePagination: any) {
		return new Promise((resolve, reject) => {
			this.http.get<APIResponse>(this.serverURILocation + "/" + device + "/" + timePagination).subscribe(
				data => {
					if(data.errorCode) {
						return reject(data.errorMessage);
					}
					return resolve(data.result);
				},
				err => {
					console.log(err);
				}
			)
		})
	}

	renameDevice(deviceId: any, newName: any) {
		return new Promise((resolve, reject) => {
			this.http.post<APIResponse>(this.serverURIDevice + "/rename/" + deviceId, {
				newName: newName
			}).subscribe(
				data => {
					if(data.errorCode) {
						return reject(data.errorMessage);
					}
					return resolve(data.result);
				},
				err => {
					console.log(err);
				}
			)
		})
	}
}
