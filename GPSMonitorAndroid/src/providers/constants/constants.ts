import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the ConstantsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ConstantsProvider {
	constants: any = {
		serverURI: 'https://gps-monitor.eu-gb.mybluemix.net'
		// serverURI: 'http://10.178.116.112:9001'
		// serverURI: 'http://localhost:9001'
		// serverURI: 'http://192.168.1.2:9001'
	}
	constructor(public http: HttpClient) {

	}
	get(key: string) {
		return this.constants[key];
	}
}
