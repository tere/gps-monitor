import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Storage } from "@ionic/storage";
import { SettingsProvider } from "../settings/settings";
import { ConstantsProvider } from "../constants/constants";
/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/


interface APIResponse {
	errorCode: number,
	errorMessage: string,
	result: any
}

@Injectable()
export class UserProvider {
	private user = {};
	private serverURI: string = "http://localhost:9001/user";
	// private serverURI: string = "http://10.178.116.112:9001/user";
	constructor(
		private storage: Storage,
		public settings: SettingsProvider,
		public http: HttpClient,
		public constants: ConstantsProvider
	) {
		console.log("Hello UserProvider Provider");
		this.serverURI = this.constants.get('serverURI') + "/user";
    }
    getLocalUserData() {
        return this.storage.get("userData").then(
            data => {
                console.log(data);
            },
            error => {
                console.log(error);
            }
        );
	}
	clearLocalStorageData(key: string) {
		this.storage.remove(key);
	}
	connect(userData: any) {
		return new Promise((resolve, reject) => {
			this.http.post<APIResponse>(this.serverURI + '/connect', {
				deviceID: this.settings.getDeviceId() || 'browser',
				userData: userData
			}).subscribe(
				data => {
					console.log(data);
					if(data.errorCode) {
						return reject(data.errorMessage);
					}
					Promise.all([
						this.settings.setUserToken(data.result.token),
						Promise.resolve(delete data.result.token),
						this.settings.setUserData(data.result)
					]).then(() => {
						return resolve(data.result);
					})
				},
				err => {
					return reject(err);
				}
			);
		});
	}
}
