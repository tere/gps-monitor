import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/observable';
import { SettingsProvider } from '../providers/settings/settings';

@Injectable()
export class ServerAuthInterceptor implements HttpInterceptor {
	token: any = 'notloggedin';
	constructor(public settings: SettingsProvider) {
		this.settings.getUserToken().then(token => {
			if(token) {
				this.token = token;
			}
		})
	}
	async getToken() {
		return await this.settings.getUserToken();
	}
	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		const authReq = req.clone({
			headers: req.headers.set('Authorization', this.settings.authHeader)
		});
		return next.handle(authReq);

	}
}
