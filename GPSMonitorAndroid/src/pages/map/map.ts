import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ToastController } from 'ionic-angular';

import {
	GoogleMaps,
	GoogleMap,
	GoogleMapsEvent,
	GoogleMapOptions,
	CameraPosition,
	MarkerOptions,
	Marker,
	Polyline
} from '@ionic-native/google-maps';
import { DevicesProvider } from '../../providers/devices/devices';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
/**
 * Generated class for the MapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
	selector: 'page-map',
	templateUrl: 'map.html',
})
export class MapPage {
	map: GoogleMap;
	mapReady: boolean = false;
	device: any;
	locations: any;
	timePagination: any = 0;
	trace: Polyline;
	refreshInterval: any = 2000;
	interval: any = null;
	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		private googleMaps: GoogleMaps,
		public toastCtrl: ToastController,
		public devices: DevicesProvider,
		public alertCtrl: AlertController
	) {
		this.device = navParams.get("device");
		console.log(this.device);
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad MapPage');
		this.loadMap();
		this.startAutoRefresh();
	}


	startAutoRefresh() {
		if(this.interval) {
			clearInterval(this.interval);
			this.interval = null;
		}
		this.interval = setInterval(() => {
			this.getDeviceLocations();
		}, this.refreshInterval);
	}

	changeRefreshInterval() {
		let changeRefreshIntervalAlert = this.alertCtrl.create({
			title: "Change refresh interval",
			subTitle: "Interval in ms for refreshing rate",
			inputs: [
				{
					name: "newRefreshInterval",
					value: this.refreshInterval,
					type: "text",
				}
			],
			buttons: [
				{
					text: "Save",
					handler: (data) => {
						this.refreshInterval = data.newRefreshInterval;
						this.startAutoRefresh();
					}
				}
			]
		});
		console.log(changeRefreshIntervalAlert);
		changeRefreshIntervalAlert.present();
	}

	loadMap() {
		// Create a map after the view is loaded.
		// (platform is already ready in app.component.ts)
		this.map = GoogleMaps.create('map_canvas', {
			camera: {
				target: {
					lat: 45.23574366532972,
					lng: 25.64144643590804
				},
				zoom: 13,
				tilt: 30
			}
		});


		// Wait the maps plugin is ready until the MAP_READY event
		this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
			this.mapReady = true;
		});
	}

	getDeviceLocations() {
		this.devices.getDeviceLocation(this.device.uuid,this.timePagination).then(locations => {
			console.log(locations);
			this.locations = locations;
			this.map.addMarker({
				position: { lat: this.locations[0].latitude,  lng: this.locations[0].longitude},
				title: this.locations[0].altitude
			});
			this.map.addMarker({
				position: { lat: this.locations[this.locations.length - 1].latitude,  lng: this.locations[this.locations.length - 1].longitude},
				title: this.locations[this.locations.length - 1].altitude
			});
			this.timePagination = this.locations[this.locations.length - 1].time;
			this.tracePolyline();
		});
	}

	tracePolyline() {
		this.map.addPolyline({
			points: this.locations.map(l => {
				return {
					lat: l.latitude,
					lng: l.longitude
				}
			}),
			width: 2
		})
	}

	onButtonClick() {
		if (!this.mapReady) {
			this.showToast('map is not ready yet. Please try again.');
			return;
		}
		this.map.clear();

		// Get the location of you
		// this.map.getMyLocation()
		// 	.then((location: MyLocation) => {
		// 		console.log(JSON.stringify(location, null, 2));

		// 		// Move the map camera to the location with animation
		// 		return this.map.animateCamera({
		// 			target: location.latLng,
		// 			zoom: 17,
		// 			tilt: 30
		// 		}).then(() => {
		// 			// add a marker
		// 			return this.map.addMarker({
		// 				title: '@ionic-native/google-maps plugin!',
		// 				snippet: 'This plugin is awesome!',
		// 				position: location.latLng,
		// 				animation: GoogleMapsAnimation.BOUNCE
		// 			});
		// 		})
		// 	}).then((marker: Marker) => {
		// 		// show the infoWindow
		// 		marker.showInfoWindow();

		// 		// If clicked it, display the alert
		// 		marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
		// 			this.showToast('clicked!');
		// 		});
		// 	});
	}

	showToast(message: string) {
		let toast = this.toastCtrl.create({
			message: message,
			duration: 2000,
			position: 'middle'
		});

		toast.present(toast);
	}
}
