import { BrowserMapPage } from './../browser-map/browser-map';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DevicesProvider } from '../../providers/devices/devices';
import { MapPage } from '../map/map';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { Platform } from 'ionic-angular/platform/platform';

/**
 * Generated class for the DevicesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
	selector: 'page-devices',
	templateUrl: 'devices.html',
})
export class DevicesPage {
	userDevices: any = [];
	isOnDevice: boolean = false;
	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public nav: NavController,
		public devices: DevicesProvider,
		public alertCtrl: AlertController,
		public platform: Platform
	) {
		this.platform.ready().then(() => {
			// Okay, so the platform is ready and our plugins are available.
			// Here you can do any higher level native things you might need.
			this.isOnDevice = this.platform.is('cordova');
		});
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad DevicesPage');
		this.getUserDevices();
	}



	getUserDevices() {
		this.devices.getAllDevices().then(devices => {
			this.userDevices = devices;
		}).catch(err => {
			console.log(err);
		})
	}

	viewOnMap(device: any) {
		this.navCtrl.push(this.isOnDevice ? MapPage : BrowserMapPage, {
			device: device
		});
	}

	renameDevice(device, i) {
		let renameAlert = this.alertCtrl.create({
			title: `Change name of: ${device.deviceName}`,
			subTitle: `Enter the new name`,
			inputs: [
				{
					name: 'deviceName',
					placeholder: 'New Device Name',
					type: 'text',
					value: device.deviceName
				}
			],
			buttons: [
				{
					text: 'Cancel',
					handler: () => {
						console.log('Cancel');
					}
				},
				{
					text: 'Save',
					handler: (data) => {
						this.devices.renameDevice(device.uuid, data.deviceName).then( result => {
							this.userDevices[i].deviceName = data.deviceName;
						})
					}
				}
			]
		});
		renameAlert.present();
	}


}
