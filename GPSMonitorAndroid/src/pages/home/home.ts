import { Events } from 'ionic-angular/util/events';
import { LoginPage } from './../login/login';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SettingsProvider } from '../../providers/settings/settings';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { SettingsPage } from '../settings/settings';
@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage {
	backgroundGeoLocationIsRunning: boolean = false;
	constructor(
		public navCtrl: NavController,
		public settings: SettingsProvider,
		public backgroundGeolocation: BackgroundGeolocation,
		public alertCtrl: AlertController,
		public events: Events
	) {
		this.events.subscribe('location:received', () => {
			this.backgroundGeoLocationIsRunning = true;
		});
	}

	stopBackgroundGeolocation() {
		let alert = this.alertCtrl.create({
			title: "Background Location tracking has been stoped",
			subTitle: "Location tracking will start again with the application start",
			buttons: ["Ok"]
		});
		this.backgroundGeolocation.stop().then(() => {
			alert.present();
		}).catch(() => {
			console.log("Hopefully not");
		})
	}
	goToRegister() {
		this.navCtrl.setRoot(LoginPage)
	}
	goToSettings() {
		this.navCtrl.setRoot(SettingsPage)
	}
	registerDevice() {
		console.log("Device uuid: ", this.settings.getDeviceId());
	}
}
