import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DevicesProvider } from '../../providers/devices/devices';
import { ViewChild, ElementRef } from '@angular/core';

/**
 * Generated class for the BrowserMapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;
@Component({
	selector: 'page-browser-map',
	templateUrl: 'browser-map.html',
})
export class BrowserMapPage {
	@ViewChild('map') mapElement: ElementRef;
	device: any;
	map: any;
	locations: any[] = [];
	timePagination: any = 0;
	interval: any = null;
	refreshInterval: any = 2000;
	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public devices: DevicesProvider,
		public alertCtrl: AlertController
	) {
		this.device = navParams.get("device");
		console.log(this.device);
	}

	initMap() {
		console.log("init map");
	}
	ionViewDidLoad() {
		console.log('ionViewDidLoad BrowserMapPage');
		this.loadMap();
	}
	startAutoRefresh() {
		if (this.interval) {
			clearInterval(this.interval);
			this.interval = null;
		}
		this.interval = setInterval(() => {
			this.getDeviceLocations();
		}, this.refreshInterval);
	}
	loadMap() {
		console.log("load map");
		let latLng = new google.maps.LatLng(-34.9290, 138.6010);

		let mapOptions = {
			center: latLng,
			zoom: 15,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		}

		this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
		this.getDeviceLocations();
	}
	changeRefreshInterval() {
		let changeRefreshIntervalAlert = this.alertCtrl.create({
			title: "Change refresh interval",
			subTitle: "Interval in ms for refreshing rate",
			inputs: [
				{
					name: "newRefreshInterval",
					value: this.refreshInterval,
					type: "text",
				}
			],
			buttons: [
				{
					text: "Save",
					handler: (data) => {
						this.refreshInterval = data.newRefreshInterval;
						this.startAutoRefresh();
					}
				}
			]
		});
		console.log(changeRefreshIntervalAlert);
		changeRefreshIntervalAlert.present();
	}
	getDeviceLocations() {
		this.devices.getDeviceLocation(this.device.uuid, this.timePagination).then((locations: any[]) => {
			console.log(locations, this.locations);
			if(locations.length == 0) return;
			this.locations = [...this.locations, ...locations];

			new google.maps.Marker({
				position: { lat: parseFloat(this.locations[0].latitude), lng: parseFloat(this.locations[0].longitude) },
				map: this.map
			});
			new google.maps.Marker({
				position: { lat: parseFloat(this.locations[this.locations.length - 1].latitude), lng: parseFloat(this.locations[this.locations.length - 1].longitude) },
				map: this.map
			});


			this.timePagination = this.locations[this.locations.length - 1].time;
			this.map.setCenter({ lat: parseFloat(this.locations[this.locations.length - 1].latitude), lng: parseFloat(this.locations[this.locations.length - 1].longitude) })
			this.tracePolyline();
		});
	}
	tracePolyline() {
		let path = this.locations.map(l => {
			return {
				lat: parseFloat(l.latitude),
				lng: parseFloat(l.longitude)
			}
		});
		console.log(path);
		new google.maps.Polyline({
			path: path,
			geodesic: true,
			strokeColor: '#FF0000',
			strokeOpacity: 1.0,
			strokeWeight: 2
		}).setMap(this.map);
	}
}
