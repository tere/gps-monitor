import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SettingsProvider } from '../../providers/settings/settings';
import { AlertController } from 'ionic-angular';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';

@Component({
	selector: 'page-settings',
	templateUrl: 'settings.html',
})
export class SettingsPage {
	public appSettings: any = {
		desiredAccuracy: 100,
		stationaryRadius: 50,
		distanceFilter: 50,
		debug: false,
		startOnBoot: false,
		stopOnTerminate: false,
		interval: 10000,
		url: "https://gps-monitor.eu-gb.mybluemix.net/location/" + this.settings.getDeviceId(),
		syncUrl: "https://gps-monitor.eu-gb.mybluemix.net/location/sync/" + this.settings.getDeviceId(),
		syncThreshold: 100,
		postTemplate: ['@latitude', '@longitude', 'foo', 'bar']
		// postTemplate: {
		// 	latitude: "@latitude",
		// 	longitude: "@longitude",
		// 	accuracy: "@accuracy",
		// 	deviceID: this.settings.getDeviceId(),
		// 	time: "@time"
		// }
	}
	constructor(private alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams, public settings: SettingsProvider, private backgroundGeolocation: BackgroundGeolocation) {

	}
	ionViewDidLoad() {
		console.log('ionViewDidLoad SettingsPage');
		this.settings.getAppSettings().then(data => {
			if (data != null) {
				this.appSettings = data;
			}
		});
	}

	saveSettings() {
		this.settings.setAppSettings(this.appSettings).then(() => {
			let alert = this.alertCtrl.create({
				title: 'Settings Saved',
				subTitle: 'Settings were saved. Please restart app for them to take effect',
				buttons: ['Ok']
			});
			alert.present();
			this.backgroundGeolocation.stop();
		})
	}


}
