import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { HomePage } from '../home/home';
import { SettingsPage } from '../settings/settings';
import { Events } from 'ionic-angular/util/events';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
	selector: "page-login",
	templateUrl: "login.html"
})
export class LoginPage {
	registerCredentials = { email: "", password: "" };
	loginMode: boolean = false;
	user: UserProvider;
	loadingCtrl: any = null;
	alertCtrl: any = null;

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		private up: UserProvider,
		public LoadingController: LoadingController,
		public AlertController: AlertController,
		public events: Events
	) {
		this.user = up;
		this.loadingCtrl = this.LoadingController.create({
			content: 'Please wait...'
		});
	}

	ionViewDidLoad() {
		console.log("ionViewDidLoad LoginPage");
		this.user.getLocalUserData().then(data => {
			console.log(data);
		})
	}

	doConnect() {
		this.loadingCtrl.present();

		this.user.connect({ email: this.registerCredentials.email, password: this.registerCredentials.password }).then(data => {
			this.events.publish('user:loggedIn');
			let alert = this.AlertController.create({
				title: 'yay',
				subTitle: 'You have been succesfully logged in',
				buttons: [{
					text: 'Go Home',
					handler: () => {
						this.navCtrl.setRoot(HomePage);
					}
				}, {
					text: 'Go to Settings',
					handler: () => {
						this.navCtrl.setRoot(SettingsPage);
					}
				}]
			});
			this.loadingCtrl.dismiss();
			alert.present();
		}).catch(reason => {
			console.log(reason);
			let alert = this.AlertController.create({
				title: 'Could not connect',
				subTitle: 'Something bad happened',
				buttons: ['Ok']
			});
			this.loadingCtrl.dismiss();
			alert.present();
		})
	}

}
