# Android GPS Monitoring System

## Components
 - Server
 - Client (Monitor & Montorizat)
 - Locatii Deploy


## Story
Printr-o aplicatie android un utilizator inregistrat poate 
 - inregistra un device pentru a fi monitorizat
 - monitoriza un device ce a fost inregistrat pentru a fi monitorizat


## User Stories (possible)
 - Ca utilizator android:
    - imi pot crea un cont (cu email si parola) pentru a putea utiliza aplicatia
    - ma pot conecta la cont folosind un email si o parola
    - ma pot conecta la un cont cu un device deja inregistrat :) 
    - pot inregistra un device pe contul deja conectat

 - Un device:
    - poate trimite date catre server cu coordonatele sale
    - poate primi date despre locatia device-urilor inregistrate

### Other: 
#### Android App
 - la apasarea unui buton va fi inregistrat pe server si va avea un ID (uman) generat
 - dupa conectarea utilizatorului va fi asociat device-ul cu contul curent. un device poate fi asociat unui singur cont
 - intr-un ecran poate fi vizualizata o harta cu device-urile inregistrate
 - atingerea unui marker poate declansa anumite actiuni pe device-ul conectat
 - poate fi setata o alerta pe distanta maxima dintre un device monitor si un device monitorizat